# > Imports
# > Discord dependencies
import discord
from discord.ext import commands

# > Standard library
import socket


class Info(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description="Get the name of my host")
    async def host(self, ctx: commands.Context):
        await ctx.respond(f"I am being hosted from *{socket.gethostname()}*")

    @commands.slash_command(
        description="Get a link to my GitLab repository"
    )
    async def repo(self, ctx: commands.Context):
        await ctx.respond("https://gitlab.com/PSJ-Sokolov/onze-bot")

    @commands.slash_command(
        description="Send me a message directly"
    )
    async def print(self, ctx: commands.Context, message: str):
        await ctx.response.defer(ephemeral=True)
        print(f"{ctx.author} sent:\n'{message}'")
        print()
        await ctx.respond("Sent message!")


def setup(bot):
    bot.add_cog(Info(bot))
