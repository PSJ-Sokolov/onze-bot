import discord
from discord.ext import commands


class Example(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description="PingPong")
    async def ping(self, ctx: commands.Context):
        """An example of a simple slash ping command"""
        # defer tells the discord API that it can wait for our output.
        # ephemeral means that only the user that typed the command is able to see the response.
        await ctx.response.defer(ephemeral=True)
        await ctx.respond("pong")


def setup(bot):
    bot.add_cog(Example(bot))
