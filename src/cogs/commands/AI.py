import requests
import discord
from discord import option
from discord.ext import commands
import re
from keybert import KeyBERT
import spacy
import spacy.cli
from transformers import GPT2Tokenizer, GPT2LMHeadModel, DistilBertTokenizerFast, DistilBertForMaskedLM, pipeline
import torch
import asyncio
import random
import time

from helper import bold, emote, fetch_env_field


SPECIAL_TOKENS = [
    "[UNHEIL_MENTION]", "[SOKO_MENTION]", "[EDRAKET_MENTION]",
    "[G1ZM0K_MENTION]", "[CASTAGNE_MENTION]", "[SIEM_MENTION]",
    "[TIES_MENTION]", "[EMOTE]", "[NEWLINE]", "[URL]"
]

MODEL_PATH_V1 = "models/GPhiT_1"
MODEL_PATH_V2 = "models/GPhiT_2"
MODEL_PATH_V3 = "models/MaskedPhiLM"
DEVICE = torch.device("cuda") if torch.cuda.is_available()\
    else torch.device("cpu")
print("Selected", DEVICE)

GOOGLE_API_KEY = fetch_env_field("GOOGLE_API_KEY")
GOOGLE_CX = fetch_env_field("GOOGLE_CX")

try:
    nlp = spacy.load("nl_core_news_sm")
except OSError:
    print("Downloading spacy model...")
    spacy.cli.download("nl_core_news_sm")
    nlp = spacy.load("nl_core_news_sm")

stop_words = set(nlp.Defaults.stop_words)


def load_GPhiT_v1():
    print("Loading GPhiT 1...")
    try:
        tokenizer = GPT2Tokenizer.from_pretrained("yhavinga/gpt2-medium-dutch")
        tokenizer.pad_token = tokenizer.eos_token
        model = GPT2LMHeadModel.from_pretrained(
            MODEL_PATH_V1, local_files_only=True, torch_dtype=torch.float16
        ).to(DEVICE)
    except FileNotFoundError as e:
        print(e)
        return None, None

    print("Done!")
    return tokenizer, model


def load_GPhiT_v2():
    print("Loading GPhiT 2...")
    try:
        tokenizer = GPT2Tokenizer.from_pretrained(MODEL_PATH_V2)
        model = GPT2LMHeadModel.from_pretrained(
            MODEL_PATH_V2, local_files_only=True, torch_dtype=torch.float16
        ).to(DEVICE)
    except FileNotFoundError as e:
        print(e)
        return None, None

    print("Done!")
    return tokenizer, model


def load_MaskedPhiLM():
    print("Loading MaskedPhiLM...")
    try:
        tokenizer = DistilBertTokenizerFast.from_pretrained(
            MODEL_PATH_V3)
        model = DistilBertForMaskedLM.from_pretrained(
            MODEL_PATH_V3, local_files_only=True, torch_dtype=torch.float16
        ).to(DEVICE)

    except FileNotFoundError as e:
        print(e)
        return None, None

    print("Done!")
    return tokenizer, model


class ArtificialIntelligence(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.tokenizer_v1, self.model_v1 = load_GPhiT_v1()
        self.tokenizer_v2, self.model_v2 = load_GPhiT_v2()
        self.emote_tokenizer, self.emote_model = load_MaskedPhiLM()
        if not self.model_v1:
            return
        if not self.model_v2:
            return
        if not self.emote_model:
            return

        self.generator_v1 = pipeline(
            "text-generation",
            model=self.model_v1,
            tokenizer=self.tokenizer_v1,
            device=0 if DEVICE == torch.device("cuda") else "cpu"
        )
        self.generator_v2 = pipeline(
            "text-generation",
            model=self.model_v2,
            tokenizer=self.tokenizer_v2,
            device=0 if DEVICE == torch.device("cuda") else "cpu"
        )
        self.emote_generator = pipeline(
            "fill-mask",
            model=self.emote_model,
            tokenizer=self.emote_tokenizer,
            device=0 if DEVICE == torch.device("cuda") else "cpu"
        )

    async def generate_text(self,
                            ctx: commands.Context,
                            model: str,
                            pipeline: pipeline,
                            prompt: str,
                            repetition_penalty: float,
                            top_p: float,
                            top_k: int,
                            temperature: float,
                            num_candidates: int):
        if not pipeline:
            print("The model for this command is not hosted on the repository.")
            await ctx.response.defer(ephemeral=True)
            await ctx.respond("My current host does not have the model loaded.")
            return

        await ctx.response.defer(ephemeral=False)

        max_new_tokens = 500 if model == "GPhiT 1" else 750

        tick = time.time()

        generated_text = pipeline(
            prompt,
            repetition_penalty=repetition_penalty,
            top_p=top_p,
            top_k=top_k,
            do_sample=True,
            temperature=temperature,
            max_new_tokens=max_new_tokens,
            pad_token_id=50256,
            num_return_sequences=num_candidates,
            no_repeat_ngram_size=4
        )

        if model == "GPhiT 1":
            scores = [score(generated_text[i]["generated_text"], self.tokenizer_v1, self.model_v1)
                      for i in range(len(generated_text))]
        elif model == "GPhiT 2":
            scores = [score(generated_text[i]["generated_text"], self.tokenizer_v2, self.model_v2)
                      for i in range(len(generated_text))]

        print(
            f"Generated {len(generated_text)} candidates in {time.time() - tick:.2f}s")
        best_output = generated_text[scores.index(
            min(scores))]["generated_text"]

        return best_output

    async def make_typing_mistake(self, response: str, mistake_chance: float):
        mistake_was_made = random.random() <= mistake_chance
        original_response = response

        if mistake_was_made:
            response_list = list(response)

            index = random.randint(0, len(response_list) - 1)
            while response_list[index] not in " abcdefghijklmnopqrstuvwxyz":
                index = random.randint(0, len(response_list) - 1)
            response_list[index] = replace_keyboard_adj(response_list[index])
            response = "".join(response_list)

        return mistake_was_made, original_response, response

    async def handle_response(self,
                              ctx: commands.Context,
                              prompt: str,
                              response: str,
                              mistake_chance: float):

        response = format_QA(response)
        response = replace_newline_tokens(response)
        response = replace_URL_tokens(response)
        response = replace_emote_tokens(
            response, self.bot, self.emote_generator)
        response = replace_mention_tokens(response, self.bot)

        [initial_response, *followup_responses] = response.split("\n")

        # Highlight the input prompt in bold.
        initial_response = bold(initial_response[:len(
            prompt)]) + initial_response[len(prompt):]

        # After sending the initial response the response.defer will stop
        # and the bot will start 'typing' the follow-up messages.
        await ctx.respond(initial_response)

        for followup_response in followup_responses:
            followup_response = followup_response.strip()
            if not followup_response:
                continue

            mistake_was_made, original_response, followup_response = await self.make_typing_mistake(
                followup_response, mistake_chance)

            async with ctx.typing():
                word_count = len(followup_response.split(" "))
                await asyncio.sleep(word_count*0.2)
                message = await ctx.send(followup_response)

            if mistake_was_made:
                await asyncio.sleep(5)
                await message.edit(content=original_response)

    @commands.slash_command(description="Ask PSJ a question")
    @option(
        "question",
        description="Enter a question to ask",
        required=True
    )
    @option(
        "temperature", input_type=float, default=0.7,
        description="The value used to module the next token probabilities. Higher values means more random."
    )
    @option(
        "repetition_penalty", input_type=float, default=1.1,
        description="The parameter for repetition penalty. 1.0 means no penalty."
    )
    @option(
        "top_p", input_type=float, default=0.73,
        description="Only the smallest set of most probable tokens are kept for generation."
    )
    @option(
        "top_k", input_type=int, default=0,
        description="The number of highest probability vocabulary tokens to keep for top-k-filtering."
    )
    @option(
        "truth_mode", input_type=bool, default=False,
        description="Whether to use the deterministic or the random model."
    )
    @option(
        "mistake_chance", input_type=float, default=0.1, min_value=0.0, max_value=1.0,
        description="De kans dat de bot een typfout maakt bij het tikken van zn antwoord."
    )
    @option(
        "num_candidates", input_type=int, default=25, min_value=1, max_value=100,
        description="The number of candidates to generate."
    )
    async def psjustice(self,
                        ctx: commands.Context,
                        question: str,
                        temperature: float,
                        repetition_penalty: float,
                        top_p: float,
                        top_k: int,
                        truth_mode: bool,
                        mistake_chance: float,
                        num_candidates: int):
        if truth_mode:
            repetition_penalty = 1.176
            top_p = 0.1
            top_k = 40
            temperature = 0.7

        question = question if question.endswith("?") else question + "?"
        question = question if question[0].isupper(
        ) else question[0].upper() + question[1:]
        prompt = f"Q: {question} A:"

        response = await self.generate_text(ctx,
                                            model="GPhiT 2",
                                            pipeline=self.generator_v2,
                                            prompt=prompt,
                                            repetition_penalty=repetition_penalty,
                                            top_p=top_p,
                                            top_k=top_k,
                                            temperature=temperature,
                                            num_candidates=num_candidates)

        await self.handle_response(ctx,
                                   prompt=question,
                                   response=response,
                                   mistake_chance=mistake_chance)

    @commands.slash_command(description="Finish a sentence PSJ-style")
    @option(
        "prompt",
        description="Enter a 'beginnetje'",
        required=True
    )
    @option(
        "repetition_penalty", input_type=float, default=1.2,
        description="The parameter for repetition penalty. 1.0 means no penalty."
    )
    @option(
        "top_p", input_type=float, default=0.98,
        description="Only the smallest set of most probable tokens are kept for generation."
    )
    @option(
        "top_k", input_type=int, default=50,
        description="The number of highest probability vocabulary tokens to keep for top-k-filtering."
    )
    @option(
        "temperature", input_type=float, default=0.7,
        description="The value used to module the next token probabilities. Higher values means more random."
    )
    @option(
        "mistake_chance", input_type=float, default=0.1, min_value=0.0, max_value=1.0,
        description="De kans dat de bot een typfout maakt bij het tikken van zn antwoord."
    )
    @option(
        "num_candidates", input_type=int, default=5, min_value=1, max_value=15,
        description="The number of candidates to generate."
    )
    async def psjank(self,
                     ctx: commands.Context,
                     prompt: str,
                     repetition_penalty: float,
                     top_p: float,
                     top_k: int,
                     temperature: float,
                     mistake_chance: float,
                     num_candidates: int):
        response = await self.generate_text(ctx,
                                            model="GPhiT 1",
                                            pipeline=self.generator_v1,
                                            prompt=prompt,
                                            repetition_penalty=repetition_penalty,
                                            top_p=top_p,
                                            top_k=top_k,
                                            temperature=temperature,
                                            num_candidates=num_candidates)

        await self.handle_response(ctx,
                                   prompt=prompt,
                                   response=response,
                                   mistake_chance=mistake_chance)


def replace_keyboard_adj(char: str):
    if char not in " abcdefghijklmnopqrstuvwxyz":
        return ""

    table = {
        " ": "@", "a": "qws", "b": "hvn", "c": "xfv",
        "e": "wsdr", "f": "rtdgc", "g": "tyfhv", "d": "ersfx",
        "h": "yugjb", "i": "uojk", "j": "uihkn", "k": "iojlm",
        "l": "pok", "m": "kn", "n": "bjm", "o": "iklp",
        "p": "ol", "q": "wa", "r": "edft", "s": "weadz",
        "t": "ryfg", "u": "yihj", "v": "cgb", "w": "qase",
        "x": "zdc", "y": "tugh", "z": "sx"
    }
    return random.choice(table[char])


def format_QA(text: str):
    # text is a string of the format "Q: ... A: ..."
    return text.replace("A: ", "\n").replace("Q: ", "")


def replace_newline_tokens(text: str):
    # this guarantees that all newline tokens will be replaced,
    # without leaving any space characters behind
    return re.sub(r'\s*\[NEWLINE\]\s*', '\n', text)


def replace_emote_tokens(text: str, bot: discord.Bot, pipeline):
    while (idx := text.find("[EMOTE]")) != -1:
        # Replace the first [EMOTE] with the model's mask token
        text = text[:idx] + "[MASK]" + text[idx + 7:]

        # Predict the token for the mask token
        prediction = pipeline(text)[0]

        # Extract emote
        emote_text = prediction['token_str'][1:-7]
        discord_emote = emote(bot, emote_text)
        if discord_emote not in bot.emojis:
            discord_emote = random.choice(list(bot.emojis))

        discord_emote_text = f"<:{discord_emote.name}:{discord_emote.id}>"

        # Replace the mask token with the predicted token
        text = text[:idx] + discord_emote_text + text[idx + 6:]

    return text


def replace_URL_tokens(text: str):
    if "[URL]" in text:
        query = text
        for token in SPECIAL_TOKENS:
            query = query.replace(token, "")

        kw_model = KeyBERT()
        keywords = kw_model.extract_keywords(
            text, keyphrase_ngram_range=(3, 3), stop_words=None, use_mmr=True, diversity=0.7)

        query = keywords[0:3]

        # Combine the top 2 keywords into a single string
        query = [keyword[0] for keyword in query]
        query = " ".join(query).split(" ")

        # Only unique words and no stopwords
        query = list(set(query) - stop_words)
        query = " ".join(query)

        # Perform the web search with the entire text as the query
        url = f"https://www.googleapis.com/customsearch/v1?key={GOOGLE_API_KEY}&cx={GOOGLE_CX}&q={query}"
        response = requests.get(url)
        response_json = response.json()

        # Extract all results and get their links
        results = response_json["items"]
        links = [result["link"] for result in results]

        # Replace each [URL] tag with the best links
        for i in range(text.count("[URL]")):
            link = f"<{links[i]}>"
            text = text.replace("[URL]", link, 1)

    return text


def replace_mention_tokens(text: str, bot: discord.Bot):
    text = text.replace("[UNHEIL_MENTION]", "<@!283336503546085376>")
    text = text.replace("[SOKO_MENTION]", "<@!114793429761785856>")
    text = text.replace("[EDRAKET_MENTION]", "<@!298029603681992704>")
    text = text.replace("[G1ZM0K_MENTION]", "<@!229322060827066369>")
    text = text.replace("[CASTAGNE_MENTION]", "<@!273436322322972683>")
    text = text.replace("[SIEM_MENTION]", "<@!174952781663305729>")
    text = text.replace("[TIES_MENTION]", "<@!195921250508341248>")
    return text


def score(text, tokenizer, model):
    if not text:
        raise ValueError("Input text should not be empty.")

    model.eval()
    model.to(DEVICE)

    # Tokenize the text
    input_ids = tokenizer.encode(
        text, truncation=True, return_tensors='pt').to(DEVICE)
    N = input_ids.shape[1]  # Number of tokens

    # Get the model's predictions
    with torch.no_grad():
        outputs = model(input_ids, labels=input_ids)

    # The loss is the cross-entropy between the labels and predictions
    loss = outputs.loss

    # Perplexity is exp(cross-entropy)
    perplexity = torch.exp(loss).item()

    # Perplexity per word
    ppw = perplexity**(1/N)

    # Freeing up GPU memory
    del input_ids, outputs, loss
    torch.cuda.empty_cache()

    return ppw


def setup(bot):
    bot.add_cog(ArtificialIntelligence(bot))
