# > Imports
# > Standard Library
from io import BytesIO
import requests
import re

# > 3rd Party Dependency
from PIL import Image, ImageEnhance

# > Discord imports
import discord
from discord import option
from discord.ext import commands


class Images(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # Make a slash command, provide an image.
    @commands.slash_command(description="Deepfry an image from URL")
    # Give it a required argument, with a description.
    @option(
        "img_url",
        description="Enter the image URL",
        required=True
    )
    async def deepfry(self, ctx: commands.Context, img_url: str):
        """Deepfries an image hosted at `img_url'"""

        # Check if the provided url hosts an image. If not back out and inform.
        if not re.match(r"(https?:\/\/.*\.(?:png|jpg|jpeg))", img_url):
            await ctx.response.defer(ephemeral=True)
            await ctx.respond("Provided Image URL did not link to an image")
            return
        
        await ctx.response.defer()

        # Get the image.
        # WARNING: Someone could craft an url that links to a file that looks like an image
        # but actually caries a harmful payload.
        response = requests.get(img_url)

        with Image.open(BytesIO(response.content)) as image:
            # Get the image and 'deepfry it'.
            image = image.copy().convert("RGB")
            image = ImageEnhance.Sharpness(image).enhance(100)
            image = ImageEnhance.Color(image).enhance(100)

            # Save image
            out = BytesIO()
            image.save(out, format="jpeg", quality=1)

            # Restore file.
            out.seek(0)

        # Respond with the image.
        await ctx.respond(file=discord.File(out, filename="deepfried.jpeg"))


def setup(bot):
    bot.add_cog(Images(bot))
