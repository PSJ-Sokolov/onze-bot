import discord
from discord import option
from discord.ext import commands
import json


def bereid(recept):
    return json.load(recept)

def roer(bordje_pasta):
    return dict(sorted(bordje_pasta.items()))


# Load data from json
with open("src/json/copypasta.json", encoding="utf-8") as recept:
    bordje_pasta = bereid(recept)
bordje_pasta = roer(bordje_pasta)

class Copypasta(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description="Send a copypasta")
    @option(
        "name",
        description="Enter the name of the copypasta",
        required=True,
        autocomplete=discord.utils.basic_autocomplete(bordje_pasta)
    )
    async def copypasta(self, ctx: commands.Context, name: str):
        sliert = name
        if sliert in bordje_pasta:
            await ctx.response.defer(ephemeral=False)
            await ctx.respond("\n".join(bordje_pasta[sliert]))
        else:
            await ctx.response.defer(ephemeral=True)
            await ctx.respond("Unknown pasta.")

def setup(bot):
    bot.add_cog(Copypasta(bot))
