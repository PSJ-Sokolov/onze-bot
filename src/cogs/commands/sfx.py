import discord
from discord import option
from discord.ext import commands
import os

# Load the names of all available sfx files on startup.
sfx_filenames = []
for file_name in os.listdir("./src/media/"):
    if file_name.endswith(".mp3"):
        sfx_filenames.append(file_name[:-4])

class SFX(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description="Send a sound effect file")
    @option(
        "name",
        description="Enter the name of the sound effect",
        required=True,
        autocomplete=discord.utils.basic_autocomplete(sfx_filenames)
    )
    async def sfx(self, ctx: commands.Context, name: str):
        if name in sfx_filenames:
            await ctx.response.defer(ephemeral=False)
            await ctx.respond(file=discord.File(f"./src/media/{name}.mp3"))
        else:
            await ctx.response.defer(ephemeral=True)
            await ctx.respond("Unknown sound effect.")

def setup(bot):
    bot.add_cog(SFX(bot))
