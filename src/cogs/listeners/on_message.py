# > Imports
# > Discord dependencies
import discord
from discord.ext import commands

# > Standard libraries
import json
import re
import random

from helper import *

# Load data from json files
with open("src/json/images.json", encoding="utf-8") as images_file:
    images = json.load(images_file)


class On_message(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.batchest_hitlist = [
            "star wars", r"rick (&|and|en) morty", "elden ring",
            "lightsaber", "marvel", r"super(held|hero)", "warcraft",
        ]

    @commands.Cog.listener()
    async def on_message(self, msg):
        """Respond to stuff that has been said in the server."""
        # Don't respond to ourselves, preventing self-reference.
        if msg.author == self.bot.user: return

        message = msg.content.lower()

        if re.search(r"^reee+", message):
            # Respond with an image of an angry pepe.
            await msg.channel.send(images["reee"])

        if "mods?" in message:
            # Respond with gif of Incidental 41 from Spongebob looking
            # leary from side to side.
            await msg.channel.send(emote(self.bot, "modCheck"))

        if re.search(r"\brecursi(ef?|on|ve)\b", message):
            # Respond with a random gif from a collection of "recursive gifs".
            await msg.channel.send(random.choice(images["recursion"]))

        if re.search(r".+(is|are|zijn) (gone|dead|weg|dood)[.!]?$", message):
            # Respond with the crabrave meme when a variant of "X is gone" is said.
            await msg.channel.send(images["crabrave"])
        
        if any(map(lambda regex: re.search(regex, message), self.batchest_hitlist)):
            await msg.channel.send(emote(self.bot, "BatChest"))
        
        if msg.channel.id == 1041641674570469437 and "https://www.youtube.com/watch?v=" in message:
            for emoji in filter_emotes(self.bot, "jerma"):
                await msg.add_reaction(emoji)


def setup(bot):
    bot.add_cog(On_message(bot))
