# > Imports
# > Discord dependencies
import discord
from discord.ext import commands

# > Standard library
import json

# Load data from json files
with open("src/json/images.json", encoding="utf-8") as images_file:
    images = json.load(images_file)


class On_member_join(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member):
        """Welcome a newly joined member by sending a welcoming image."""
        # Get the system channel (the channel in a server where Discord sends special messages).
        channel = member.guild.system_channel
        if channel:
            await channel.send(images["welcome"])


def setup(bot):
    bot.add_cog(On_member_join(bot))
