#!/usr/bin/env python

import aioconsole
from discord.ext import commands
import discord
import asyncio
import socket
import dotenv
import random
import json
import sys
import os

from helper import fetch_env_field, italics

# Load data from json files
with open("src/json/quotes.json", encoding="utf-8") as quotes_file:
    quotes = json.load(quotes_file)

bot = commands.Bot(intents=discord.Intents.all())

SKIP_LOAD_AI_COG = "--no-ai" in sys.argv
SKIP_START_END_MESSAGE = "--silent" in sys.argv or "-s" in sys.argv
# Check if we're running in test mode,  quite hacky.
# basically run as a different bot added to a different server if we're running
# in test mode.
TEST_MODE = "--test" in sys.argv or "-t" in sys.argv
CURRENT_GUILD = 1048361798560137347 if TEST_MODE else 279682148225646593
BOTSHELL = 1049409754214187049 if TEST_MODE else 326446250754834433


@bot.event
async def on_ready():
    activity = discord.Activity(
        type=discord.ActivityType.watching, name=f"from {socket.gethostname()}"
    )
    await bot.change_presence(status=discord.Status.online, activity=activity)

    server_profile = bot.get_guild(CURRENT_GUILD).get_member(bot.user.id)
    await server_profile.edit(nick="Onze Bot")

    botshell = discord.utils.get(bot.get_all_channels(), id=BOTSHELL)
    if not TEST_MODE and not SKIP_START_END_MESSAGE:
        await botshell.send(italics(random.choice(quotes['openers'])))

    print(f"Logged in as {bot.user} - {bot.user.id}")
    print("------")


def load_folder(name: str):
    """Loads commands (called cogs) in folders and adds them to the bot."""
    print(f"Loading {name}...")
    for file_name in os.listdir(f"./src/cogs/{name}"):
        if file_name.endswith(".py"):
            if file_name == "AI.py" and SKIP_LOAD_AI_COG:
                continue
            try:
                bot.load_extension(f"cogs.{name}.{file_name[:-3]}")
                print("- Loaded:", file_name)
            except discord.ExtensionAlreadyLoaded:
                print("- Cog already loaded:", file_name)
            except discord.ExtensionNotFound:
                print("- Cog not found:", file_name)
    print(f"Loaded {name}!")


async def stdin_listener():
    channel = None
    while True:
        message = await aioconsole.ainput()
        if not message:
            continue

        if message.isnumeric():
            potential_new_channel = discord.utils.get(
                bot.get_all_channels(), id=int(message)
            )
            if potential_new_channel:
                channel = potential_new_channel
                print("Chatting channel updated to: " + channel.name)
                continue

        if channel:
            await channel.send(message)


async def termination():
    botshell = discord.utils.get(bot.get_all_channels(), id=BOTSHELL)
    if not TEST_MODE and not SKIP_START_END_MESSAGE:
        await botshell.send(italics(random.choice(quotes['closers'])))
    await bot.change_presence(status=discord.Status.invisible)


async def main(token: str):
    await asyncio.gather(bot.start(token), stdin_listener())


if __name__ == "__main__":
    load_folder("commands")
    load_folder("listeners")
    # TODO: load_folder("loops")

    token = (
        fetch_env_field("TEST_LOGIN_TOKEN")
        if TEST_MODE
        else fetch_env_field("LOGIN_TOKEN")
    )

    # Main event loop
    try:
        bot.loop.run_until_complete(main(token))
    except discord.LoginFailure:
        print("Could not log in!")
        dotenv.unset_key(".env", "LOGIN_TOKEN")
        print("Cleared stored login token..!")
    except KeyboardInterrupt:
        print("\nCaught interrupt signal.")
    except Exception as e:
        print(e)
    finally:
        print("Exiting...")
        bot.loop.run_until_complete(termination())
        bot.loop.close()
        sys.exit(0)
