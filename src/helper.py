import dotenv
import discord
import sys


def italics(string: str):
    return f"*{string}*"


def bold(string: str):
    return f"**{string}**"


def underline(string: str):
    return f"__{string}__"


def strikethrough(string: str):
    return f"~~{string}~~"


def code_block(string: str):
    return f"```{string}```"


def quote(string: str):
    return f">{string}"


def spoiler(string: str):
    return f"||{string}||"


def emote(bot: discord.Bot, name: str):
    emoji_list = bot.get_guild(279682148225646593).emojis
    return next(
        (emoji for emoji in emoji_list if emoji.name == name), name
    )


def filter_emotes(bot: discord.Bot, search: str):
    emoji_list = bot.get_guild(279682148225646593).emojis
    return list(filter(lambda emoji: search.lower() in emoji.name.lower(), emoji_list))


def fetch_env_field(key: str, prompt=""):
    """A utility function to extract data from a .env file,
    prompt the user for a value if it is missing"""
    if key in dotenv.dotenv_values():
        return dotenv.get_key(".env", key)
    else:
        prompt = f"Enter {key.lower().replace('_', ' ')}:" if not prompt\
            else prompt
        try:
            value = input(prompt + " ")
        except KeyboardInterrupt:
            print("\nReceived keyboardinterupt, aborting..")
            # Set the exit code to 128 + SIGINT (=2),
            # to adhere to UNIX conventions:
            sys.exit(130)
        dotenv.set_key(".env", key, value)
        return value
