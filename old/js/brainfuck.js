class syntaxError {
    constructor (message) {
        this.message = message
    }
}

const MAX_TIME = 5000;
const DISPLAY_CELLS = 10;

function visualizeTape(pointer, tape, output) {
    for(let i = 0; i < DISPLAY_CELLS; i++) {
        if(i == pointer) output += `|${tape[i]}|`;
        else             output += ` ${tape[i]} `;
    }
    output += '\n';
    return output;
}

class Brainfuck {
    constructor(tape=[], pointer=0) {
        this.tape    = tape;
        this.pointer = pointer;
    }
    
    execute(program) {
        //shenanigans detection apparatus
        const startTime = new Date();
        let output = '';
        let counter = 0;
        let n = 0;

        //initialize the visible part of the tape
        for(let i = 0; i < DISPLAY_CELLS; i++) {
            if(this.tape[i] == undefined) this.tape[i] = 0;
        }

        output = visualizeTape(this.pointer, this.tape, output);

        while(counter < program.length) {
            switch (program[counter]) {
                case '+':
                    this.tape[this.pointer]++;
                    output = visualizeTape(this.pointer, this.tape, output);
                    break;
                case '-':
                    this.tape[this.pointer]--;
                    output = visualizeTape(this.pointer, this.tape, output);
                    break;
                case '<':
                    if(this.pointer > 0) this.pointer--;

                    if(this.tape[this.pointer] == undefined) {
                        this.tape[this.pointer] = 0;
                    }
                    output = visualizeTape(this.pointer, this.tape, output);
                    break;
                case '>':
                    this.pointer++;
                    if(this.tape[this.pointer] == undefined) {
                        this.tape[this.pointer] = 0;
                    }
                    output = visualizeTape(this.pointer, this.tape, output);
                    break;
                case '[':
                    if(this.tape[this.pointer] == 0) {
                        n = 1;
                        do {
                            if(counter > program.length) {
                                throw new syntaxError('Missing closing bracket.');
                            }
                            counter++;
                            if     (program[counter] == '[') n++;
                            else if(program[counter] == ']') n--;
                        } while (n != 0);
                    }
                    break;
                case ']':
                    n = 0;
                    do {
                        if(startTime.getTime() + MAX_TIME <  (new Date()).getTime()) throw new SyntaxError('Leuk geprobeerd Siem...');

                        if(counter < 0) {
                            throw new SyntaxError('Missing opening bracket.');
                        }
                        if     (program[counter] == '[') n++;
                        else if(program[counter] == ']') n--;
                        counter--; 
                    } while(n != 0)
                    break;
            }
            counter++;
        }
        return output;
    }

    reset() {
        this.tape = [];
        this.pointer = 0;
    }
}

module.exports = Brainfuck;