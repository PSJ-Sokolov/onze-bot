#!/usr/bin/env node
// Onze bot -- A discord bot for "Onze server"
// Copyright (C) PSJ Sokolov 2019-2020
// Copyright (C) Tim Koorsntra 2019-2020
// Copyright (C) Siem Kleuskens 2019-2020
// Copyright (C) Casper Fabritius 2019-2020

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful, // but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//>Imports
const Discord  = require('discord.js');
const fetch    = require('node-fetch');
const System   = require('os');
const execSync = require('child_process').execSync;
const BrainfuckInterpreter = require('./brainfuck.js');

//>Load json data
// Put the bots authentication token in a seperate file 'auth.json' which we
// do not include directly in the code so otherwise people could hijack our bot
const config    = require('../json/config.json');
const auth      = require('../json/auth.json');
const quotes    = require('../json/quotes.json');
const copypasta = require('../json/copypasta.json');
const hatebase  = require('../json/hatebase.json');
const images    = require('../json/images.json');
const music     = require('../json/top2000.json');

//>Global Values
const message_limit     = 2000;
const announce_precence = true;
const client            = new Discord.Client();
const brainfuck         = new BrainfuckInterpreter();
const stdin             = process.openStdin();

// Check missing API tokens:
if        (!auth.POSTMAN_TOKEN) {
    console.log('[WARNING!]: MISSING POSTMAN TOKEN!');
}
if (!auth.IMGUR_CLIENT_ID) {
    console.log('[WARNING!]: MISSING IMGUR TOKEN!');
}

let n_counter      = 0;
let bully_counter  = 0;
let hacker_counter = 0;
let activeChannel;
let retard_id;
let cursed_id;

//>Classes
class SimpleQueue {
    constructor(length=10) {
        this.length = length;
        this._inner_array = [];
    }

    push(object) {
        if (this._inner_array.push(object) > this.length) {
            this._inner_array.shift(-1);
        }
    }

    occurences(object) {
        return this._inner_array.filter(x => x == object)
            .length;
    }
}
class SpamChecker extends SimpleQueue {
    constructor(limit = 3, length = 10) {
        super(length);
        this.limit = limit;
    }

    isSpam(text) {
        return this.occurences(text) > this.limit;
    }

    checkSpam(text) {
        this.push(text);
        return this.isSpam(text);
    }
}
let spam_checker = new SpamChecker;

//>Helper Functions
// Generate random integer between max and min (by default it's [1,100])
const randInt = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);
// Return a random element from a supplied array
const randElem = iterable => iterable.length > 0 ? iterable[randInt(0, iterable.length-1)] : null;

const randHexColour = () => {
    let result = '#';
    for(let i = 0; i < 6; i++) result += randElem('0123456789abcdef');
    return result;
}

const mentionToId = mention => mention.replace(/[^0-9]/g, '');

const mentionIsUser = mention => /^<@!?[0-9]+>$/.test(mention);

const mentionIsRole = mention => /^<@&[0-9]+>$/.test(mention);

const mentionIsChannel = mention => /^<#[0-9]+>$/.test(mention);

const quantify = (n, single, plural=single+'s') => n + ' ' + (n == 1 ? single : plural);

const bMemify = string => string.replace(/([bpfg]|ph)/gi, '🅱️');

const ridicule = string => (string.toLowerCase().includes('o'))
    ? string.replace(/o/gi, '<:OMEGALUL:633831513078890507>')
    : `${string} <:OMEGALUL:633831513078890507>`;

const codeFormat = (string, language='') => '```' + language + '\n' + string + '```';

const createEmbed = (message, title, description, ...fields) => new Discord.RichEmbed()
    .setTitle(title)
    .setAuthor(message.guild.members.get(client.user.id).displayName, client.user.displayAvatarURL)
    .addField(...fields)
    .setColor('#cd0000')
    .setDescription(description)
    .setTimestamp()
    .setFooter('Bot created by Tim Koornstra, Philip Sokolov, Casper Fabritius', client.user.displayAvatarURL);

// SUB ROUTINES WITH SIDE EFFECTS
const ENV_PRE = config.ENV_PRE || 'ONZE_BOT_';
const LOOKUP  = what => process.argv.slice(2).includes(`--${what}=TRUE`) ||
                        process.env[`${ENV_PRE}${what}`] === 'TRUE' ||
                        config[what];
const LOG_REF = config.LOG_STREAM === 'STDERR' ? console.error : console.log; 
const LOGOID  = stream => what => pre => msg => LOOKUP(what)? stream(pre, msg) : void msg;
const LOGS    = what => LOGOID(LOG_REF)(what)(`[${what}:] `);
const DEBUG   = LOGS('DEBUG');
const WARN    = LOGS('WARN');
const BIG_BROTHER = LOGS('BIG_BROTHER')
// TEST
DEBUG('WE ARE DEBUGGING');
DEBUG(LOOKUP('DEBUG'));
DEBUG(`BIG BROTHER IS TURNED: ${LOOKUP('BIG_BROTHER')}`);
WARN('WE ARE WARNING');

function ADD_HOSTNAME(guild) {
    // DECLERATIONS
    const safeLastIndexOf       = (s1, s2) => s1.lastIndexOf(s2) === -1 ? s1.length : s1.lastIndexOf(s2);
    const returnBeforeLast      = (s1, s2) => s1.substring(0, safeLastIndexOf(s1, s2));
    const appendIfShorterThan   = nat => (s1, s2) => (s1 + s2).length < nat ? s1 + s2 : s1;
    const appendIfShorterThan32 = appendIfShorterThan(32);
    const OUR_BOT_REF           = guild.members.get(client.user.id);
    const NAME                  = returnBeforeLast(client.user.tag, '#');
    const NICKNAME              = OUR_BOT_REF.nickname || NAME; // Gore lisp/sh taktiek
    const NEW_NICKNAME          = appendIfShorterThan32(NICKNAME, `@${System.hostname()}`);
    
    DEBUG(NICKNAME);
    DEBUG(NEW_NICKNAME);
    WARN(NICKNAME === NEW_NICKNAME);
    //SIDE EFFECT
    OUR_BOT_REF.setNickname(NEW_NICKNAME);
    return;
}

//>OnLoad
client.once('ready', () => {
    // Set the activity: show prefix and hostname.
    client.user.setActivity(`'${config.prefix}' @${System.hostname()}`, { type: 'LISTENING' });
    // Set a nickname: use the previous nickname (with the part after the last '@' chopped off) + the hostname.) 
    client.guilds.forEach(ADD_HOSTNAME);
    console.log(`Logged in as ${client.user.tag}!`);
    console.log(`prefix is '${config.prefix}'`);
    // List servers the bot is connected to
    console.log('Servers:')

    // Allows comparison between properties of objects
    const comparoid = getter => {
        return (obj_a, obj_b) => {
            const a = getter(obj_a);
            const b = getter(obj_b);
            return a > b ? 1 : (a < b ? -1 : 0);
        }
    }
    const sorts = (data, getters) => (!getters.length ? data : sorts(data.sort(comparoid(getters[0])), getters.slice(1)));

    client.guilds.forEach(guild => {
        console.log(`  ${guild.id} - ${guild.name} (${quantify(guild.memberCount, 'member')}) AS: "${guild.members.get(client.user.id).nickname || client.user.tag}"`);

        // List all channels
        // guild.channels.sort(comparoid([x => x.type, x => x.type.length])).forEach(channel => {
        guild.channels.filter(channel => channel.type == 'category').forEach(guildChannel => {
            console.log(`    ├── ${guildChannel.id} (${guildChannel.type})\t- ${guildChannel.name}`);
            sorts(guildChannel.children, [x => x.type, x => x.type.length]).forEach(channel => {
                let output = `    │   └─ ${channel.id} (${channel.type})\t- ${channel.name}`;
                if (channel.type == 'voice' && channel.members.size != 0) {
                    output += `    (${quantify(channel.members.size, 'member')})`;
                }
                console.log(output);
            });
        });

        // Separate servers visually.
        console.log("    ▇\n");
    });

    console.log('... WE\'RE LIVE!');

    activeChannel = client.channels.get(config.botkanaal_id);

    if (announce_precence) {
        client.channels.get(config.botkanaal_id).send(`ON PREFIX: "${config.prefix}" : ${randElem(quotes.openers)}`);
    }
});

//>All prefixed bot commands
client.on('message', async message => {
    if (LOOKUP('LISTEN_TO_JUST_THE_BOTSHELL') && message.channel.id != config.botkanaal_id) {
        return;
    }
    if (!message.content.startsWith(config.prefix)) {
        return;
    }

    let separated;
    if (message.content.startsWith(config.prefix + ' ')) {
        separated = message.content.slice(config.prefix.length+1).split(' ');
    } else if (message.content.startsWith(config.prefix)) {
        separated = message.content.slice(config.prefix.length).split(' ');
    } else {
        separated = [];
    }

    const command = separated[0];
    const args    = separated.slice(1);

    if        (command == 'host') {
        message.channel.send(`I am being hosted from *${System.hostname()}*`);
    } else if (command == 'repo') {
        message.channel.send('https://gitlab.com/PSJ-Sokolov/onze-bot');
    } else if (command == 'ARGV') {
        message.channel.send(process.argv.slice(2));
    } else if (command == 'compile') {
        // Leef omdat een andere bot hetzelfde prefix deelt.
    } else if (command == 'clone') {
        message.channel.send(codeFormat('git clone https://gitlab.com/PSJ-Sokolov/onze-bot'));
    } else if (command == 'ping') {
        message.channel.send('pong');
    } else if (command == 'pong') {
        message.channel.send('ping');
    } else if (command == 'beep') {
        message.channel.send('boop');
    } else if (command == 'boop') {
        message.channel.send('omega');
    } else if (command == 'marco') {
        message.channel.send('polo!');
    } else if (command == 'nig') {
        message.channel.send('nog!');
    } else if (command == 'pestgedrag') {
        if (!retard_id) {
            message.channel.send('Er is nog geen persoon om te pesten.');
            return;
        }
        if (bully_counter == 0) {
            message.channel.send(`${retard_id} is nog niet gepest! Begin nu met pesten!`)
            return;
        }

        message.channel.send(`${retard_id} is al ${bully_counter} keer gepest.`);
    } else if (command == 'hacker') {
        hacker_counter++;
        message.channel.send(`<@${config.sam_id}> called people a hacker ${quantify(hacker_counter, 'time')}`);
    } else if (command == 'insult') {
        message.channel.send(randElem(hatebase));
    } else if (command == 'pasta') {
        const title = args.join(' ');
        if (title != '') {
            for (let key in copypasta) {
                if (title.toLowerCase() == key) {
                    message.channel.send(copypasta[key]);
                    return;
                }
            }
            message.channel.send('Unknown copypasta');
        } else {
            let output = '';
            Object.keys(copypasta).forEach(pasta => {
                output += `- ${pasta}\n`;
            });
            message.channel.send(output);
        }
    } else if (command == 'stand') {
        let output = '';
        const n = args[0] ? args[0] : 1;
        for (let i = 0; i < n; i++) {
            output += `『${randElem(music)[randInt(0,1)]}』\n`;
        }
        message.channel.send(output);
    } else if (command == 'ssrep' || command == 'SSREP') {
        if (args.length == 0) message.channel.send(`Missing argument for "${command}"`);
        console.log(`SERVER SIDE REPRESENTATION: ${args[0]}`);
        message.reply(codeFormat(args[0]), 'Java');
    } else if (command == 'pispaal') {
        if (!args[0] && !retard_id) {
            message.channel.send('niemand is \'em momenteel.');
            return;
        }
        if (!args[0]) {
            message.channel.send(`${retard_id} is 'em momenteel.`);
            return;
        }
        if (!mentionIsUser(args[0])) {
            message.channel.send('This is not a user!');
            return;
        }
        if (args[0] == retard_id) {
            message.channel.send(`${retard_id} wordt al gepest.`);
            return;
        }
        if (mentionToId(args[0]) == config.gijs_id) {
            // Kijk of we Cumbrecht niet gaan pesten, want die jongen die trekt dit niet langer meer zo!
            // Straks gaat hij er nog een einde aan maken...
            message.channel.send('Zaadbrecht kan helemaal niet gepest worden!');
            return;
        }

        retard_id = args[0];
        bully_counter = 0;
        message.channel.send(`Ok, ${retard_id} gaat nu gepest worden.`);
    } else if (['name','rename','nick','nickname'].includes(command)) {
        if (!args[0]) {
            message.channel.send('Geef als eerste argument wiens naam ik moet aanpassen bitte');
            return;
        }
        if (!args[1]) {
            message.channel.send('Ik kan mensen toch niet *geen* naam geven????');
            return;
        }

	if (args[0] === 'server') { // Eigenlijk wel best rorted dat we die argumenten shiften maar dat kunnen we niet meer aanpassen
             const nieuwe_server_naam = args.slice(1).join(" ");
             message.reply(`Ok de server gaat nu ${nieuwe_server_naam} genoemd worden`);
	     message.guild.setName(nieuwe_server_naam);
             return;
	}

        const target_id = mentionToId(args[0]);
        if (mentionIsUser(args[0])) {
            message.guild.members.get(target_id).setNickname(args.slice(1).join(" "))
            .then(() => message.channel.send(`Ok, ${args[0]} gaat vanaf nu '${args.slice(1).join(" ")}' genoemd worden.`))
                    .catch(() => message.channel.send(`Tja discord is een fascistisch stuk rotzooi en geeft me niet de permissies om de naam van ${args[0]} aan te passen.`));
        } else if (mentionIsRole(args[0])) {
            message.guild.roles.get(target_id).setName(args.slice(1).join(" "))
            .then(() => message.channel.send(`Ok, ${args[0]} gaat vanaf nu '${args.slice(1).join(" ")}' genoemd worden.`))
                    .catch(() => message.channel.send(`Tja discord geeft me niet de permissies om de naam van ${args[0]} an te passen.`));
        } else {
            message.channel.send(`'${args[0]}' is noch de naam van een gebruiker noch de naam van een rol!`);
            return;
        }
    } else if (command == 'curse' || command == 'vervloek') {
        // Vervloek iemand en laat de bot diegene dan de hele tijd na praten.
        if (args.length == 0) {
            message.channel.send(`Missing argument for "${command}"`);
            return;
        }
        cursed_id = mentionToId(args[0]);
        console.log(`Cursed_Id ${cursed_id}`);
    } else if (command == 'prefix') {
        if (args.length == 0) {
            message.channel.send(`Missing argument for "${command}"`);
            return;
        }

        config.prefix = args.join(' ');
        console.log(`The new prefix is: "${config.prefix}"`);
        client.user.setActivity(config.prefix, { type: 'LISTENING' });
        message.channel.send(`Ok, the new prefix is: "${config.prefix}"`);
    } else if (['echo','print','log','say'].includes(command)) {
        if (args.length == 0) message.channel.send(`Missing argument for "${command}"`);
        message.channel.send(`"${args.join(' ')}"`)
    } else if (command == 'brainfuck') {
        if (args[0] == 'reset') {
            message.channel.send('OK, resetting the state of the tape!');
            brainfuck.reset();
        } else {
            try {
                message.channel.send(codeFormat(brainfuck.execute(args.join(' '))));
            } catch(e) {
                message.channel.send(e.message);
            }
        }
    } else if (command == 'ridicule' || command == 'lmao') {
        message.channel.send(ridicule(args.join(' ')));
    } else if (['img','image','plaatje','afbeelding'].includes(command)) {
        try {
            const query = args[0] == 'nsfw' ? args.slice(1).join('+') : args.join('+');

            const req = await fetch(
                `https://api.imgur.com/3/gallery/search/${args[0]=='nsfw' ? '' : 'score'}?q=${query}`,
                {
                    method: 'GET',
                    headers: {
                        'Postman-Token': auth.POSTMAN_TOKEN,
                        'cache-control': 'no-cache',
                        'Authorization': `Client-ID ${auth.IMGUR_CLIENT_ID}`
                    }
                }
            );
            const res = await req.json();
            //if nsfw was specified, filter the results
            let data = randElem(res.data.filter(item => item.nsfw || args[0] != 'nsfw'));
            //Post the image
            if (data.is_album && data.images.length > 1) {
                message.channel.send('This image is part of an album. View the whole album at: ' + data.link);
            } else if (data.is_album) {
                let parsedImage = new Discord.Attachment(data.images[0].link);
                message.channel.send(parsedImage);
            } else {
                let parsedImage = new Discord.Attachment(data.link);
                message.channel.send(parsedImage);
            }
        } catch (err) {
            message.channel.send('There are no images matching your query.');
        }
    } else if (['roll','rand','random','rnd'].includes(command)) {
        //Dit is een implementatie van het /roll commando uit World of Warcraft
        let upper = 100;
        let lower = 1;
        if (args.length >= 2) {
            upper = args[1];
            lower = args[0];
        } else if (args.length == 1) {
            upper = args[0];
        }
        message.channel.send(`${message.author} rolls ${randInt(lower,upper)} (${lower}-${upper})`);
    } else if (command == 'rtd' || command == 'dice') {
        const diceCount = args[0] > 0 ? args[0] : 5;
        let result = [];
        for(let i = 0; i < diceCount; i++) {
            result.push(randInt(1,6));
        }
        message.channel.send(`${message.author} rolls [${result.join(', ')}]`);
    } else if (command == 'role') {
        try {
            if (args[0] == 'list') {
                if (!args[1]) {
                    const allRoles = Array.from(message.guild.roles).map(item => item.toString().split(',')[1]);
                    const embed = createEmbed(message, 'Roles', 'A summation of the roles on this server.', 'Roles', allRoles.join('\n'));
                    message.channel.send(embed);
                } else if (args[1] == 'members') {
                    const roleName = args.slice(2).join(' ');
                    const role = message.guild.roles.find(rol => rol.name === roleName);
                    const allMembers = Array.from(role.members).map(item => item.toString().split(',')[1]);
                    const embed = createEmbed(message, 'Users', 'A summation of the users in this role.', 'Members', allMembers.join('\n'));
                    message.channel.send(embed);
                } else {
                    message.channel.send(`This command was not recognised. Use one of our our other ${config.prefix}role list commands instead.`);
                }
            } else if (mentionIsUser(args[1]) && args[2]) {
                const member = message.mentions.members.first();
                const role = message.guild.roles.find(rol => rol.name === args.slice(2).join(' '));
                if (args[0] == 'give') {
                    if (role.position < member.highestRole.position) {
                        member.addRole(role);
                        message.channel.send(`${member} now has the role '${role.name}'`);
                    } else {
                        message.channel.send("The role you want to assign is higher than the one(s) you currently have.");
                    }
                } else if (args[0] == 'leave') {
                    if (role && role.position < member.highestRole.position) {
                        member.removeRole(role);
                        message.channel.send(`${member} has been removed from role '${role.name}'`);
                    } else if (role.position >= member.highestRole.position){
                        message.channel.send("You cannot remove a role with a position higher than or equal to your highest role.");
                    } else {
                        message.channel.send("This user does not have this role.");
                    }
                }
            } else if (args[0] == 'remove') {
                const roleName = args.slice(1).join(' ');
                const role = message.guild.roles.find(rol => rol.name === roleName);
                if (role && !role.members.size) {
                    role.delete();
                    message.channel.send(`Role '${role.name}' has now been removed.`);
                } else if (role.members.size > 0) {
                    message.channel.send(`There are still members in this role. Remove them first.`);
                } else {
                    message.channel.send(`Role '${role.name}' does not exist.`);
                }
            } else if (args[0] == 'create') {
                const role = args.slice(1).join(' ');
                message.guild.createRole({
                    name: role,
                    color: randHexColour(),
                });
                message.channel.send(`Role ${role} has been created.`);
            } else {
                message.channel.send(`This command was not recognised. Use one of our our other ${config.prefix}role commands instead.`);
            }
        }
        catch (err) {
            console.log('Error:', err);
            if(err instanceof RangeError) {
                message.channel.send('There are too many roles to be listed right now. Remove at least one to see this list.');
            } else {
                message.channel.send('OOPSIE WOOPSIE!!! UwU We made a fucky wucky. The code monkeys at our headquawtews are wowking VEWY HAWD to fix it!!!!!1!.');
            }
        }
    } else if (command === 'branch') {
        message.channel.send(execSync('git branch').toString().split('\n')[0].slice(2));  
    } else if (command === 'skip') {
	let times        = Number(args[0]);
	let skip_command = args[1] || '!s';
	for (let i = 0; i < times; i++) {
		message.channel.send(skip_command);
	}
    } else if (command === 'kys') {
	message.reply(`HELP HELP <@!${message.author.id}> is trying to kill me!`)
        client.user.setActivity(`Door ${message.author.username} vermoord worden`, { type: 'PLAYING' });
	await new Promise(resolve => setTimeout(resolve, 10000));
        client.channels.get(config.botkanaal_id).send(`OFF: ${randElem(quotes.famous_last_words)} (Killed by: @${message.author.username})`).then(() => {
            client.user.setStatus('invisible').then(() => {
	        console.log(message.author.username, 'heeft me vermoord');
                process.exit();
            });
        });
 
    } else if (command === 'fortune') {
        message.channel.send('```' + execSync('fortune -a').toString() + '```');  
    } else {
        console.log(`wrong command "${command}"`);
        message.reply(`I do not know the command "${command}".`);
    }
});

//>All miscellaneous message listeners
client.on('message', message => {
    if (message.author.id == client.user.id) {
        return;
    }
    const text = message.content.toLowerCase();

    if (message.isMemberMentioned(client.user)) {
        console.log(`${message.channel.name}: ${message.content}`);
    }
    if (text.includes('cs?')) {
        const insult = randElem(hatebase.filter(insult => insult.endsWith('s')));
        message.guild.roles.get(config.csgorole_id).setName(`csgo ${insult}`);
        message.channel.send(`<@&${config.csgorole_id}>, iemand interesse?`);
    }
    if (text.includes('hoeveel nachtjes')) {
        const now = new Date();
        if (now.getMonth() == 11 && now.getDate() == 5) {
            message.channel.send('Vandaag is de dag!');
            return;
        }

        const nextSinterklaasYear = (now.getMonth() == 11 && now.getDate() > 5) ? now.getFullYear()+1 : now.getFullYear();
        const sinterklaas = new Date(nextSinterklaasYear, 11, 5);
        const diffMs = sinterklaas.getTime() - now.getTime();
        const diffDays = Math.ceil(diffMs / 1000 / 3600 / 24);
        message.channel.send(`Nog ${quantify(diffDays, 'nachtje')} slapen tot Sinterklaas.`);
    }
    if (text.includes('retard')) {
        bully_counter++;
        if (!retard_id) {
            message.channel.send('Er is nog geen pispaal.');
        } else if (mentionToId(retard_id) == message.author.id) {
            message.reply('who you talking to, bitch boy?');
        } else {
            message.reply(`did you mean ${retard_id} instead?`);
        }
    }
    if (text.includes('yeezy yeezy')) {
        message.channel.send(copypasta['yeezy']);
    }
    if (text.includes('mods?')) {
        message.channel.send(copypasta['mods']);
    }
    if (text.includes('climate change is a hoax')) {
        message.channel.send(bMemify('Ok Boomer'));
    }
    if (/n+i+gg+(e+r+|a+h*)/.test(text)) {
        n_counter += text.match(/n+i+g+(e+r+|a+h*)/gi).length;
        message.channel.send(`N-word has been said ${quantify(n_counter, 'time')}...`);
    }
    if (/^reee+/g.test(text)) {
        message.channel.send({files: [images['reee']]});
    }
    if (/\brecursi(ef?|on|ve)\b/.test(text)) {
        message.channel.send({files: [randElem(images['recursion'])]});
    }
    // Als iemand vervloekt is laten we de bot de hele tijd diens berichten overnieuw spammen.
    if (message.author.id == cursed_id) {
        message.channel.send(message.content);
    }
    if (/(is|are|zijn) (gone|dead|weg|dood)[.!]?$/.test(text)) {
        message.channel.send({files: [images['crabrave']]});
    }
    /*if (spam_checker.checkSpam(text)) {
        message.reply(randElem(config.spam_responses));
    }*/
    if (text.includes('epic') || text.includes('episch')) {
        message.channel.send('gamer moment');
    }
    if (text.includes('kanye')) {
        message.channel.send(`Hey, <@${(config.sam_id)}>! You might find this interesting!`);
    }
    if (text.includes('bruh')) {
        message.channel.send('moment');
    }
    if (text.includes('<:jonko_klappen:564501973370404865> ?')) {
        message.channel.send('<@&641387682915942420>, dus wanneer jonko klappen?');
    }
    if (text.includes('🤔')) {
        message.reply('please refrain from using 🤔, and use <:thonking:727516296605990944> instead');
    }
    if (message.channel.name == 'cnn' && /https?:\/\/\w{2,}/.test(text)) {
        message.channel.send('<:monkarona:682579481264783363>');
    }
    if (text.includes('gang gang')) {
        message.delete("Dit bericht bevatte een ongezond gehalte aan cringe.");
    }
    BIG_BROTHER(`<${message.author.id},${message.author.username}> says in: <${message.channel.name},${message.channel.id}> the following:\n ${text}`);
});

//>Miscellaneous events
client.on('guildMemberAdd', member => {
    member.guild.channels.get(config.defaultkanaal_id).send({files: [images['welcome']]});
});

client.on('emojiCreate', emoji => {
    const channel = emoji.guild.channels.get(config.emotekanaal_id);
    channel.send(`Bois, we hebben een nieuwe emote: ${emoji}!`);
    channel.send(emoji.toString());
});

//Mechanische Turk
stdin.addListener('data', data => {
    const input = data.toString().trim();

    if (input == '') return;

    if (client.channels.get(input)) {
        activeChannel = client.channels.get(input);
        console.log(`Active channel is updated to: ${activeChannel.name}`);
    } else {
        activeChannel.send(input);
    }
});

//>OnKill
process.on('SIGINT', () => {
    console.log('Caught interrupt signal');
    if (announce_precence) {
        client.channels.get(config.botkanaal_id).send(`OFF: ${randElem(quotes.famous_last_words)}`).then(() => {
            client.user.setStatus('invisible').then(() => {
                console.log('exiting...');
                process.exit();
            });
        });
    } else {
        client.user.setStatus('invisible').then(() => {
            console.log('exiting...');
            process.exit();
        });
    }
});


// Login to discord with the bots token.
client.login(auth.token);

