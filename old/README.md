# onze-bot

In deze git repository wordt de source code voor *onze-bot* gehost, die op *onze server* 
draait.  De bot is geschreven in node.js met behulp van het framework 
[Discord.js](https://discord.js.org/#/), de belangrijkste file en main entry point
in het programma is ``index.js``.

# Hier zijn wat tutorials en documentatie voor het schrijven van discord bots (JS)
- https://discord.js.org/#/docs/main/stable/general/welcome
- https://discordjs.guide/#before-you-begin
- https://www.writebots.com/how-to-make-a-discord-bot/
- https://www.writebots.com/how-to-make-a-discord-bot/

# Kleine tutorial voor het gebruiken van git

## Hier zijn een paar tutorials die mensen al geschreven hebben:
- https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/
- https://try.github.io/
- https://learngitbranching.js.org/ (Dit gaat over branching)

Maar als je echt goed wilt leren hoe je git moet gebruiken, dan kun je het beste
het git book lezen: https://git-scm.com/book/en/v2 , hij is er ook in het
nederlands: https://git-scm.com/book/nl/v2.

# Hoe je de bot zelf kan draaien
Voor het draaien van de bot moet je node.js downloaden, ofwel op [linux](https://nodejs.org/en/download/package-manager/)
(bv. [ubuntu](https://phoenixnap.com/kb/install-latest-node-js-and-nmp-on-ubuntu)),
ofwel op [windows](https://nodejs.org/en/download/current/).

Daarna moet je de source code bemachtigen met dit commando:
```
git clone https://gitlab.com/PSJ-Sokolov/onze-bot
```
om het in je huidige directory te zetten, of:
```
git clone https://gitlab.com/PSJ-Sokolov/onze-bot [BESTEMMING]
```
om het op de plaats [BESTEMMING] neer te zetten.

Zodra je node hebt gedownload, ga je naar de directory waar je de repository
naartoe hebt gecloned en typ je:
```
node index.js
```

Als je wil dat de bot wordt opgepakt door onze server heb je de authenticatie
token nodig, omdat je met de authenticatie token allerlei permissies aan kan
passen en de server kan volspammen, moet
je de authenticatie token neerzetten in ``auth.js``. Dit bestand staat ook in de
``.gitingnore`` zodat ie niet wordt ingechekt in git en dus niet online komt te staan.

Als je de authentiecatie token van deze bot wil hebben, zodat je hem zelf kan
draaien, vraag het aan Philip.

<!-- TEST DE WEBHOOK -->
