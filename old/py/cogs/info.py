import socket

from discord.ext import commands

class Info(commands.Cog):    
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def host(self, ctx):
        """
        Get the name of the machine where onze-bot is currently running.
        
        Returns:
            The name of the machine where onze-bot is currently running.
        """
        await ctx.send(f'I am being hosted from *{socket.gethostname()}*')

    @commands.command()
    async def repo(self, ctx):
        """
        Get a link to the repository of onze-bot.
        
        Returns:
            A link of the repository of onze-bot.
        """
        await ctx.send('https://gitlab.com/PSJ-Sokolov/onze-bot')
    
    @commands.command()
    async def clone(self, ctx):
        """
        Get the command for cloning the repository.
        
        Returns:
            The git command for cloning the repository.
        """
        await ctx.send('```git clone https://gitlab.com/PSJ-Sokolov/onze-bot```')

def setup(bot):
    bot.add_cog(Info(bot))