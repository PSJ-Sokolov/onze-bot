import os
import json
import re
import random

from discord.ext import commands

# Load data from json files
os.chdir('..')
with open('json/images.json') as images_file:
    images = json.load(images_file)
with open('json/config.json') as config_file:
    config = json.load(config_file)

class Listeners(commands.Cog):    
    def __init__(self, bot):
        self.bot = bot
    
    @commands.Cog.listener()
    async def on_member_join(self, member):
        channel = member.guild.system_channel
        if channel: await channel.send(images['welcome'])
    
    @commands.Cog.listener()
    async def on_guild_emojis_update(self, guild, before, after):
        # Check if an emoji was added
        if len(after) > len(before):
            for channel in guild.channels:
                if channel.id == int(config['emotekanaal_id']):
                    emoji = after[-1]
                    await channel.send(f'Boys, we hebben een nieuwe emote: {emoji}!')
                    await channel.send(emoji)
            

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author == self.bot.user: return

        if re.search(r'^reee+', message.content):
            await message.channel.send(images['reee'])

        if 'mods?' in message.content:
            await message.channel.send(images['?'])

        if re.search(r'\brecursi(ef?|on|ve)\b', message.content):
            await message.channel.send(random.choice(images['recursion']))

        if re.search(r'.+(is|are|zijn) (gone|dead|weg|dood)[.!]?$', message.content):
            await message.channel.send(images['crabrave'])

def setup(bot):
    bot.add_cog(Listeners(bot))