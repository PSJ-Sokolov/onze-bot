from discord.ext import commands

class Test(commands.Cog):    
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['debug'])
    async def test(self, ctx):
        """
        Command for testing whatever.

        Returns:
            'dit is een test'
        """
        await ctx.send('dit is een test')

    @commands.command()
    async def ping(self, ctx):
        """
        Pong.

        Returns:
            'Pong!' followed by the bots ping.
        """
        await ctx.send(f'Pong! {int(self.bot.latency*1000)}ms')

    @commands.command(aliases=['print','log','say','repeat'])
    async def echo(self, ctx, text):
        """
        Make the bot repeat.

        Args:
            text: Something for the bot to say.
            (handles spaces by wrapping in quotes)

        Returns:
            text
        """
        await ctx.send(text)

def setup(bot):
    bot.add_cog(Test(bot))