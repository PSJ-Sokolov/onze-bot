from PIL import Image, ImageEnhance
from io import BytesIO
import requests

import discord
from discord.ext import commands

class Images(commands.Cog):    
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command(aliases=['compress'])
    async def deepfry(self, ctx, image_url):
        response = requests.get(image_url)
        with Image.open(BytesIO(response.content)) as image:
            image = image.copy().convert('RGB')
            image = ImageEnhance.Sharpness(image).enhance(100)
            image = ImageEnhance.Color(image).enhance(100)
            out = BytesIO()
            image.save(out, format='jpeg', quality=1)
            out.seek(0)

        await ctx.send(file=discord.File(out, filename='deepfried.jpeg'))
def setup(bot):
    bot.add_cog(Images(bot))