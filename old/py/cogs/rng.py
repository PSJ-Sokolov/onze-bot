import random, collections

from discord.ext import commands

class RNG(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['coin'])
    async def toss(self, ctx):
        """
        Flips a coin.

        Returns:
            'Heads' or 'Tails'.
        """
        result = 'Heads' if random.randint(0, 1) else 'Tails'
        await ctx.send(result)

    @commands.command(aliases=['random', 'rand'], usage='[[lower=1] upper=100]')
    async def roll(self, ctx, arg1:int=None, arg2:int=None):
        """
        Rolls a random number between [lower, upper].

        Args (optional):
            arg1 (optional): The lower range.
            arg2           : The upper range.

        Returns:
            A random number between param1 and param2. If arg1 is not specified,
            returns a random number between 1 and arg2. If neither arg1 nor
            arg2 are specified, returns a random number between 1 and 100.

        """
        if arg1 == None and arg2 == None:
            lower, upper = 1, 100
        elif arg2 == None:
            lower, upper = 1, arg1
        else:
            lower, upper = arg1, arg2

        if lower > upper:
            lower, upper = upper, lower

        result = random.randint(lower, upper)
        roller = ctx.message.author.name
        await ctx.send(f'{roller} rolled {result} ({lower} - {upper})')

    @commands.command(aliases=['rtd'])
    async def dice(self, ctx, dice_count:int=5):
        """
        Rolls a number of dice equal to dice_count.

        Args:
            dice_count (optional): the number of dice to be rolled.

        Returns:
            The result of dice_count rolls of the dice,
            formatted by number of times each result was rolled.
        """
        formattedResult = '\n'.join([f'{ogen} times a {aantal}' for aantal, ogen in sorted(collections.Counter([random.randint(1,6) for i in range(dice_count)]).items())])
        await ctx.send(f'{ctx.message.author.name} rolled:\n```{formattedResult}```')


def setup(bot):
    bot.add_cog(RNG(bot))
