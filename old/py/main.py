#!/usr/bin/env python3

#### Onze Bot -- A discord bot for "Onze server"
####
#### This program is free software: you can redistribute it and/or modify
#### it under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or
#### (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#### GNU Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <http://www.gnu.org/licenses/ >.

##> Imports
#> Standard Library
import sys
import os
import asyncio
import json
import socket

#> 3rd Party Dependancies
import discord
from discord.ext import commands


##> Code
bot = commands.Bot(command_prefix='>', intents=discord.Intents.all())

@bot.event
async def on_ready():
    activity = discord.Activity(type=discord.ActivityType.listening, name=f'\'{bot.command_prefix}\'')
    await bot.change_presence(status=discord.Status.online, activity=activity)
    await bot.get_guild(279682148225646593).get_member(bot.user.id).edit(nick=f'onze-bot@{socket.gethostname()}')

    print(f'Logged in as {bot.user}!')
    print(f'prefix is \'{bot.command_prefix}\'.')

    # Print all channels of all servers the bot is connected to
    print('Servers:')
    for guild in bot.guilds:
        print(f'{guild.id} - {guild.name} ({len(guild.members)} members)')
        for category, channels in guild.by_category()[:-1]:
            print(f'    ├── {category.id} (category)\t- {category.name}')
            for channel in channels[:-1]:
                print(f'    |   ├─ {channel.id} ({channel.type})\t- {channel.name}')
            # Handle the last channel slightly differently
            channel = channels[-1]
            print(f'    |   └─ {channel.id} ({channel.type})\t- {channel.name}')
        print(f'    ├── {category.id} (category)\t- {category.name}')
        # Handle the last category slightly differently
        category, channels = guild.by_category()[-1]
        print(f'    └── {category.id} (category)\t- {category.name}')
        for channel in channels[:-1]:
            print(f'        ├─ {channel.id} ({channel.type})\t- {channel.name}')
        # Handle the last channel slightly differently
        channel = channels[-1]
        print(f'        └─ {channel.id} ({channel.type})\t- {channel.name}')
        print()


    print('... WE\'RE LIVE!')

@bot.command()
async def prefix(ctx, *, new_prefix):
    """
    Change the prefix that onze-bot responds to.
    
    Args:
        param1: The new prefix.
    
    Returns:
        The bot will now listen to the specified prefix.
    """
    bot.command_prefix = new_prefix
    print(f'The new prefix is: "{bot.command_prefix}"')
    await ctx.send(f'Ok, the new prefix is: "{bot.command_prefix}"')
    activity = discord.Activity(type=discord.ActivityType.listening, name=f'\'{bot.command_prefix}\'')
    await bot.change_presence(status=discord.Status.online, activity=activity)

#> Main
if __name__ == '__main__':
    # Load all files from the 'cogs' folder
    for filename in os.listdir('./cogs'):
        if filename.endswith('.py'):
            print('Loading:', filename)
            bot.load_extension(f'cogs.{filename[:-3]}')
    print()

    # Load the authentication token from 'auth.json'
    with open('../json/auth.json') as auth_file:
        auth = json.load(auth_file)
    
    try:
        bot.run(auth['token'])
    except KeyboardInterrupt: print()
    finally: sys.exit(0)
